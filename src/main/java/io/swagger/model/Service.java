package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Service
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-03-06T17:21:55.950Z")


public class Service   {
  @JsonProperty("id")
  private Integer id = 0;

  @JsonProperty("name")
  private String name = "";

  @JsonProperty("description")
  private String description = "";

  @JsonProperty("lastUpdate")
  private String lastUpdate = "";

  @JsonProperty("version")
  private String version = "";

  @JsonProperty("validFor")
  private String validFor = "";

  @JsonProperty("href")
  private String href = "";

  public Service(Integer id, String name, String description, String lastUpdate, String version, String validFor, String href) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.lastUpdate = lastUpdate;
    this.version = version;
    this.validFor = validFor;
    this.href = href;
  }

  public Service() {

  }


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getLastUpdate() {
    return lastUpdate;
  }

  public void setLastUpdate(String lastUpdate) {
    this.lastUpdate = lastUpdate;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getValidFor() {
    return validFor;
  }

  public void setValidFor(String validFor) {
    this.validFor = validFor;
  }

  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  @Override
  public String toString() {
    return "Service{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", description='" + description + '\'' +
            ", lastUpdate='" + lastUpdate + '\'' +
            ", version='" + version + '\'' +
            ", validFor='" + validFor + '\'' +
            ", href='" + href + '\'' +
            '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Service service = (Service) o;
    return Objects.equals(id, service.id) && Objects.equals(name, service.name) && Objects.equals(description, service.description) && Objects.equals(lastUpdate, service.lastUpdate) && Objects.equals(version, service.version) && Objects.equals(validFor, service.validFor) && Objects.equals(href, service.href);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, description, lastUpdate, version, validFor, href);
  }
}

