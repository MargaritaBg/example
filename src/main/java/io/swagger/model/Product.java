package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Product
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-03-06T17:21:55.950Z")


public class Product   {
  @JsonProperty("service")
  private Object service = new Service();

  @JsonProperty("price")
  private Long price = 2l;

  public Product(Object service, Long price) {
    this.service = service;
    this.price = price;
  }

  public Product() {

  }


  public Object getService() {
    return service;
  }

  public void setService(Object service) {
    this.service = service;
  }

  public Long getPrice() {
    return price;
  }

  public void setPrice(Long price) {
    this.price = price;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Product product = (Product) o;
    return Objects.equals(service, product.service) && Objects.equals(price, product.price);
  }

  @Override
  public int hashCode() {
    return Objects.hash(service, price);
  }
}

