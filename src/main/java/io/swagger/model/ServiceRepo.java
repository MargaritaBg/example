package io.swagger.model;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;


@Component
@Repository
public interface ServiceRepo extends MongoRepository<Service,String> {

 Service findById(Integer id);

}
