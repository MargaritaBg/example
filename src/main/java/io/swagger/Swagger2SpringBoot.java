package io.swagger;

import io.swagger.model.Service;
import io.swagger.model.ServiceRepo;
import io.swagger.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackages = { "io.swagger", "io.swagger.api" , "io.swagger.configuration"})
public class Swagger2SpringBoot implements CommandLineRunner {

    @Autowired
    ServiceRepo serviceRepo;

    @Override
    public void run(String... arg0) throws Exception {
        if (arg0.length > 0 && arg0[0].equals("exit")) {
            throw new ExitException();
        }

        serviceRepo.deleteAll();

        Service service1 = new Service();
        service1.setId(1);
        service1.setDescription("cdn_service");
        service1.setHref("link");
        service1.setLastUpdate("05/03/2021");
        service1.setVersion("3");
        service1.setValidFor("30/03/2021");



        Service service2 = new Service();

        service2.setId(2);
        service2.setDescription("cdn_service");
        service2.setHref("link");
        service2.setLastUpdate("05/01/2021");
        service2.setVersion("2");
        service2.setValidFor("30/01/2021");


        Service service3 =new Service();

        service3.setId(3);
        service3.setDescription("cdn_service");
        service3.setHref("link");
        service3.setLastUpdate("02/02/2021");
        service3.setVersion("2");
        service3.setValidFor("30/02/2021");

        serviceRepo.save(service1);
        serviceRepo.save(service2);
        serviceRepo.save(service3);


    }

    public static void main(String[] args) throws Exception {
        new SpringApplication(Swagger2SpringBoot.class).run(args);
    }

    class ExitException extends RuntimeException implements ExitCodeGenerator {
        private static final long serialVersionUID = 1L;

        @Override
        public int getExitCode() {
            return 10;
        }

    }
}
