package io.swagger.api;

import io.swagger.model.Product;
import io.swagger.model.Service;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import io.swagger.model.ServiceRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-03-06T17:21:55.950Z")

@Controller
public class ServiceApiController implements ServiceApi {

    private static final Logger log = LoggerFactory.getLogger(ServiceApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;
    String gs = "";

    @org.springframework.beans.factory.annotation.Autowired
    public ServiceApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @Autowired
   private ServiceRepo serviceRepo;

    Product product = new Product();

//---------------------------------------------------POST---------------------------------------------------------

    @PostMapping(path = "/service", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Product> addService(@ApiParam(value = "Pet object that needs to be added to the store" ,required=true )  @Valid @RequestBody Service service) {

        if (request != null) {



            serviceRepo.save(service);

         //   Product product = new Product();

            product.setService(service);
            product.setPrice(20l);

            System.out.println("Hello");
            log.info("margarita");

                return new ResponseEntity<Product>(product,HttpStatus.OK);
            }


        return new ResponseEntity<Product>(product,HttpStatus.OK);
    }

//-------------------------------------------------------------------------------------------------------------



//------------------------------------GET-------------------------------------------------//
ArrayList<Service> list = new ArrayList<>();

    @GetMapping(path = "/service", consumes = "application/json", produces = "application/json")
    public ResponseEntity<List<Service>> findProduct() {


        if (request != null) {




            list.add(serviceRepo.findById(1));
            list.add(serviceRepo.findById(2));
            list.add(serviceRepo.findById(3));

            System.out.println("World");

            gs = new Gson().toJson(list);

                return new ResponseEntity<List<Service>>(list,HttpStatus.OK);

        }

        return new ResponseEntity<List<Service>>(list,HttpStatus.OK);
    }


 //-----------------------------------------------------------------------------------------------------------------------------------------------------

}
